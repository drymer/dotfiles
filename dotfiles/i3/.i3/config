# -*- mode: i3wm-config -*-

set $color_bad = #CC0000
set $color_degraded = #EDD400
set $color_good = #73D216
set $mod Mod4

# Fuente para títulos de ventanas
font pango:DejaVu Sans Mono 10

# Usar Ratón+$mod para mover las ventanas flotantes
floating_modifier $mod

# Display

# volver a la misma ventana que estaba apretando el mismo worskpace
workspace_auto_back_and_forth yes

# abrir una terminal
bindsym $mod+Return exec alacritty

# cerrar ventana
bindsym $mod+Shift+Q kill

# Reinicia i3
bindsym $mod+Shift+r restart

# Cambiar foco
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# Cambiar foco con las flechas
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Mover de workspace
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# Mover de workspace usando las flechas
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# Cambiar la orientación en horizontal
bindsym $mod+b split h

# Cambiar la orientación en vertical
bindsym $mod+v split v

# Fullscreen
bindsym $mod+f fullscreen

# Cambiar layout
bindsym $mod+y layout stacking
bindsym $mod+a layout default
bindsym $mod+s layout tabbed

# Cambiar entre tiling / floating
bindsym $mod+Shift+space floating toggle

# Cambiar foco entre tiling / floating windows
bindsym $mod+space focus mode_toggle

# Definir workspaces
set $ws1 1<big></big>
set $ws2 2<big></big>
set $ws3 3<big></big>
set $ws4 4<big></big>
set $ws5 5<big></big>
set $ws6 6<big></big>
set $ws7 7<big></big>
set $ws8 8<big></big>
set $ws9 9<big></big>
set $ws10 10<big></big>

# Cambiar a workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# Mover a workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# Workspace a monitores
workspace $ws1 output $display1
workspace $ws2 output $display1
workspace $ws3 output $display1
workspace $ws4 output $display1
workspace $ws5 output $display2
workspace $ws6 output $display2
workspace $ws7 output $display2
workspace $ws8 output $display2
workspace $ws9 output $display2
workspace $ws10 output $display2

# Cambiar tamaño de ventana
mode "Resize" {
        # These bindings trigger as soon as you enter the resize mode
        bindsym h resize shrink width 2 px or 2 ppt
        bindsym j resize grow height 2 px or 2 ppt
        bindsym k resize shrink height 2 px or 2 ppt
        bindsym l resize grow width 2 px or 2 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt


        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "Resize"

# Barra principal
bar {
    font pango:Dyuthi3, Awesome, Icons 10, DejaVu Sans Mono for Powerline 10
    status_command /usr/bin/i3blocks
    position top
    strip_workspace_numbers yes
    i3bar_command i3bar -t

    colors {
            background #000000
            separator  #000000

            ###XXX: Details    border  backgr. text
            focused_workspace  #990099 #222222 #729FCF
            inactive_workspace #222222 #222222 #729FCF
            active_workspace   #222222 #222222 #729FCF
            urgent_workspace   #FF0000 #222222 #FF0000
    }
}

# Bloquear pantalla
bindsym $mod+Control+l exec --no-startup-id ~/.i3/scripts/lock, mode "default"

# Seleccionar padre
bindsym $mod+i focus parent

# Seleccionar hijo
bindsym $mod+o focus child

# evitar titulos feos
new_window pixel

# Colors de las ventanas
# class                    border     backgr   text     indicator
client.focused	           #990099    #222222  #ffffff  #729FCF
client.focused_inactive    #222222    #222222  #ffffff  #729FCF
client.unfocused           #222222    #222222  #ffffff  #729FCF
client.urgent              #FF0000    #8C5665  #ffffff  #FF0000

# Wallpaper
exec --no-startup-id feh --bg-scale ~/.compartido/Imagenes/arch.jpg

# Autostart
exec --no-startup-id bash ~/.xinitrc
exec --no-startup-id bash ~/.i3/scripts/autolock.sh
exec --no-startup-id parcellite
exec --no-startup-id compton
exec --no-startup-id dunst
exec --no-startup-id xcape
exec --no-startup-id redshift -l 40.25:03.45 -t 5700:3600 -g 0.8 -m randr
exec --no-startup-id pactl set-sink-volume 0 50%
exec --no-startup-id setxkbmap us -variant altgr-intl
exec --no-startup-id firefox
exec --no-startup-id emacs
exec --no-startup-id nextcloud

# Mover programas automáticamente a workspace
assign [class="(?i)firefox"] $ws2
assign [class="(?i)tor"] $ws2
assign [class="(?i)chromium"] $ws2
assign [class="(?i)qutebrowser"] $ws2
assign [title="Steam"] $ws7
assign [title="spotify"] $ws7
assign [class="(?i)mumble"] $ws8
assign [class="(?i)transmission"] $ws8
assign [class="(?i)thunderbird"] $ws9
assign [title="mutt"] $ws9

# Hacer ventanas flotantes automáticamente
for_window [window_role="Preferences"] floating enable
for_window [window_role="help-browser"] floating enable
for_window [class="(?i)xcalc"] floating enable
for_window [window_role="pop-up"] floating enable
for_window [window_role="About"] floating enable
for_window [class="(?i)wicd"] floating enable
for_window [class="(?i)feh"] floating enable
for_window [class="Progreso de operación de archivo"] floating enable
for_window [title="textarea"] floating enable
for_window [title="qutebrowser-editor"] floating enable
for_window [title="Dispositivos Bluetooth"] floating enable
for_window [title="SM-J710F"] floating enable

# i3-gaps
smart_borders on

# Popups en pantalla completa
popup_during_fullscreen smart

# El foco no lo decide el ratón
focus_follows_mouse no

# Scratchpad
bindsym $mod+Shift+p move scratchpad
bindsym $mod+p scratchpad show

# keybinds varios
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5%
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5%
bindsym XF86AudioMute exec --no-startup-id ~/.i3/scripts/audio-mute
bindsym XF86AudioMicMute exec --no-startup-id ~/.i3/scripts/microphone-mute

# arrancar launcher
bindsym $mod+d exec --no-startup-id zsh -c "PATH=$PATH:$HOME/bin rofi -show run"
# emacs anywhere
bindsym $mod+e exec --no-startup-id zsh -c "$HOME/.emacs_anywhere/bin/run"
