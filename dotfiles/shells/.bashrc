# Path
PATH="/usr/local/bin:/usr/bin:/bin:/usr/games:/usr/sbin:/sbin:/usr/local/bin"

# Prompt verde
#PS1='\[\e[0;92m\]\u\[\e[0;93m\] % \[\e[0;92m\]\h \[\e[1;93m\]\w\[\e[0;92m\]\$ '
# Prompt azul
PS1='\[\e[1;94m\]\u\[\e[0m\] % \[\e[1;91m\]\h \[\e[1;93m\]\w\[\e[1;34m\] $\[\033[0m\] '

# Terms típicas de emacs
[[ $TERM == "dumb" ]] && TERM=xterm; PS1='$ '
[[ $TERM == "emacs" ]] && PS1='$ '
[[ $TERM == "xterm" ]] && PS1='$ '

# Term
if [ "$TERM" = "" -o "$TERM" = "unknown" ]; then
 TERM=linux
fi

# Notificar correo nuevo
if [ -x /usr/bin/biff ]; then
 biff y 2> /dev/null
fi

# Usar scripts en /etc/profile.d/
for profile_script in /etc/profile.d/*.sh ; do
  if [ -x $profile_script ]; then
    . $profile_script
  fi
done
unset profile_script

# Alias
source ~/.aliases

# Python
export WORKON_HOME=~/.virtualenvs
source /usr/local/bin/virtualenvwrapper_lazy.sh

export EDITOR="emacsclient -t -c"
export ALTERNATE_EDITOR="nano"

ESC="\033"
NORMAL="\[$ESC[m\]"
RESET="\[$ESC[${DULL};${FG_WHITE};${BG_NULL}m\]"

if [ -f /etc/bash_completion ]; then
 . /etc/bash_completion
fi
PATH=$PATH:~/Scripts/bin:$HOME/.npm-bin/bin/
PATH=$PATH:~/Scripts/bin:$HOME/bin

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
sleep 0.5

# --httptoolkit--
# This section will be reset each time a HTTP Toolkit terminal is opened
if [ -n "$HTTP_TOOLKIT_ACTIVE" ]; then
    # When HTTP Toolkit is active, we inject various overrides into PATH
    export PATH="/opt/http-toolkit/resources/app/httptoolkit-server/overrides/path:$PATH"

    if command -v winpty >/dev/null 2>&1; then
        # Work around for winpty's hijacking of certain commands
        alias php=php
        alias node=node
    fi
fi
# --httptoolkit-end--
# --httptoolkit--
# This section will be reset each time a HTTP Toolkit terminal is opened
if [ -n "$HTTP_TOOLKIT_ACTIVE" ]; then
    # When HTTP Toolkit is active, we inject various overrides into PATH
    export PATH="/opt/http-toolkit/resources/app/httptoolkit-server/overrides/path:$PATH"

    if command -v winpty >/dev/null 2>&1; then
        # Work around for winpty's hijacking of certain commands
        alias php=php
        alias node=node
    fi
fi
# --httptoolkit-end--
# --httptoolkit--
# This section will be reset each time a HTTP Toolkit terminal is opened
if [ -n "$HTTP_TOOLKIT_ACTIVE" ]; then
    # When HTTP Toolkit is active, we inject various overrides into PATH
    export PATH="/home/ap/.local/share/httptoolkit-server/client/0.1.27/overrides/path:$PATH"

    if command -v winpty >/dev/null 2>&1; then
        # Work around for winpty's hijacking of certain commands
        alias php=php
        alias node=node
    fi
fi
# --httptoolkit-end--
[[ -s "/home/ap/.gvm/scripts/gvm" ]] && source "/home/ap/.gvm/scripts/gvm"
export ELB_NAME='DEV-PORTAL-ELB-12-27-507'
export ASG_NAME='DEV-PORTAL-ASG-12.27.507-2021-06-25T12-13-36Z'
