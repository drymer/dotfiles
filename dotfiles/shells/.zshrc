# Debug
# set -x
# zmodload zsh/zprof

# Ruta de oh-my-zsh
export ZSH=$HOME/.oh-my-zsh

# Instala cosas
if [[ ! -a ~/Instalados/zsh-interactive-cd ]]; then
  git clone https://github.com/changyuheng/zsh-interactive-cd ~/Instalados/zsh-interactive-cd
fi


# Modify path
PATH="/usr/local/bin:/usr/bin:/bin:/usr/games:/usr/sbin:/sbin:/usr/local/bin"
export PATH="$PATH:$HOME/bin/"
export PATH="$PATH:$HOME/.bin/"
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:/$HOME/.emacs.d/bin/"
export PATH="$PATH:/opt/go/bin"
export PATH="$PATH:/opt/brew/bin/"
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
export GOPATH=/opt/go

# Cargar los programas instalados
source $HOME/Instalados/zsh-interactive-cd/zsh-interactive-cd.plugin.zsh
source /usr/share/z/z.sh
# fzf: automatically selects the item if there's only one
export FZF_CTRL_T_OPTS="--select-1 --exit-0"

# fzf The following example uses tree command to show the entries of the directory
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200'"

# Full command on preview window
export FZF_CTRL_R_OPTS="--preview 'echo {}' --preview-window down:3:hidden:wrap --bind '?:toggle-preview'"

# Term
export TERM="xterm-256color"

# Prompt ZSH
eval "$(starship init zsh)"

# No comprobar si hay actualizaciones
DISABLE_AUTO_UPDATE="true"

# No corregir errores
ENABLE_CORRECTION="false"

# Mostrar puntos rojos mientras se espera el completado
COMPLETION_WAITING_DOTS="true"

# Formato de tiempo
HIST_STAMPS="dd/mm/yyyy"

# Plugins
plugins=(git colored-man-pages ssh-agent daemons-aws gitfast ksp)

export MANPATH="/usr/local/man:$MANPATH"

# Usar oh-my-zsh
source $ZSH/oh-my-zsh.sh

# Por algún motivo...
# https://github.com/ohmyzsh/ohmyzsh/issues/4607#issuecomment-510494971
export FPATH="/usr/local/share/zsh/functions:$FPATH"

# Lenguaje
export LANG=es_ES.UTF-8
export LANGUAGE=es_ES.UTF-8
export LC_ALL=es_ES.UTF-8

# Editor preferido
export EDITOR="emacsclient -t -c"
export ALTERNATE_EDITOR="vim"

# Python virtualenv
export WORKON_HOME=$HOME/.virtualenvs

# Cosas de Systemd
export XDG_RUNTIME_DIR="/run/user/$UID"
export DBUS_SESSION_BUS_ADDRESS="unix:path=${XDG_RUNTIME_DIR}/bus"

# Arduino IDE path
export ARDUINO_DIR=/opt/arduino-1.8.7

source $HOME/.functions
source $HOME/.aliases
source $HOME/.fzf.zsh

# No comprobar mails
MAILCHECK=0

# Krew
#
