AddPackage asciidoctor # An implementation of AsciiDoc in Ruby
AddPackage asciinema # Record and share terminal sessions
AddPackage aws-cli # Universal Command Line Interface for Amazon Web Services
AddPackage ethtool # Utility for controlling network drivers and hardware
AddPackage exo # Application library for Xfce
AddPackage fbreader # An e-book reader for Linux
AddPackage fd # Simple, fast and user-friendly alternative to find
AddPackage feh # Fast and light imlib2-based image viewer
AddPackage fzf # Command-line fuzzy finder
AddPackage gnu-netcat # GNU rewrite of netcat, the network piping application
AddPackage gnuplot # Plotting package which outputs to X11, PostScript, PNG, GIF, and others
AddPackage gparted # A Partition Magic clone, frontend to GNU Parted
AddPackage gpick # Advanced color picker written in C++ using GTK+ toolkit
AddPackage iftop # Display bandwidth usage on an interface
AddPackage imagemagick # An image viewing/manipulation program
AddPackage jq # Command-line JSON processor
AddPackage multitail # Lets you view one or multiple files like the original tail program
AddPackage nmap # Utility for network discovery and security auditing
AddPackage pass # Stores, retrieves, generates, and synchronizes passwords securely
AddPackage prettier # An opinionated code formatter for JS, JSON, CSS, YAML and much more
AddPackage proxychains-ng # A hook preloader that allows to redirect TCP traffic of existing dynamically linked programs through one or more SOCKS or HTTP proxies
AddPackage ripgrep # A search tool that combines the usability of ag with the raw speed of grep
AddPackage rofi # A window switcher, application launcher and dmenu replacement
AddPackage rpmextract # Script to convert or extract RPM archives (contains rpm2cpio)
AddPackage socat # Multipurpose relay
AddPackage stress # A tool that stress tests your system (CPU, memory, I/O, disks)
AddPackage tcpdump # Powerful command-line packet analyzer
AddPackage tmux # A terminal multiplexer
AddPackage tor # Anonymizing overlay network.
AddPackage torsocks # Wrapper to safely torify applications
AddPackage unclutter # A small program for hiding the mouse cursor
AddPackage unp # A script for unpacking a wide variety of archive formats
AddPackage wipe # Secure file wiping utility
AddPackage wireshark-qt # Network traffic and protocol analyzer/sniffer - Qt GUI
AddPackage youtube-dl # A command-line program to download videos from YouTube.com and a few more sites
AddPackage z # Tracks your most used directories, based on 'frecency'
AddPackage zsh # A very advanced and programmable command interpreter (shell) for UNIX
IgnorePackage --foreign aconfmgr-git # A configuration manager for Arch Linux
AddPackage --foreign autojump # A faster way to navigate your filesystem from the command line
AddPackage --foreign aws-iam-authenticator-bin # A tool to use AWS IAM credentials to authenticate to a Kubernetes cluster
AddPackage exa # rich ls
AddPackage --foreign mkpasswd # Tool for creating password hashes suitable for /etc/shadow
AddPackage --foreign mongodb-bin # A high-performance, open source, schema-free document-oriented database
AddPackage --foreign mongodb-tools-bin # The MongoDB tools provide import, export, and diagnostic capabilities.
AddPackage --foreign nerd-fonts-fira-code # Patched font Fira (Fura) Code from the nerd-fonts library
AddPackage --foreign pcapfix # A tool of repairing your broken pcap and pcapng files
AddPackage --foreign toilet # free replacement for the FIGlet utility.
AddPackage zsh-completions # Additional completion definitions for Zsh
AddPackage zsh-syntax-highlighting # Fish shell like syntax highlighting for Zsh
AddPackage --foreign howdoi # Instant coding answers via the command line
AddPackage --foreign inframap # Read your tfstate or HCL to generate a graph specific for each provider, showing only the resources that are most important/relevant
AddPackage --foreign mdl # MarkDown Less is a Markdown displayer
AddPackage --foreign proselint # A linter for prose
AddPackage --foreign scrcpy # Display and control your Android device
AddPackage --foreign wordnet-cli
AddPackage --foreign mkpasswd
AddPackage --foreign mongodb-bin
AddPackage --foreign mongodb-tools-bin
AddPackage --foreign pcapfix
AddPackage --foreign toilet
