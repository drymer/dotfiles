AddPackage armagetronad # A Tron Clone in 3D.
AddPackage calibre # Ebook management application
AddPackage chromium # A web browser built for speed, simplicity, and security
AddPackage discord # All-in-one voice and text chat for gamers that's free and secure.
AddPackage firefox # Standalone web browser from mozilla.org
AddPackage gimp # GNU Image Manipulation Program
AddPackage gnucash # Personal and small-business financial-accounting application
AddPackage gajim # Full featured and easy to use XMPP (Jabber) client
AddPackage mpc # Minimalist command line interface to MPD
AddPackage mpd # Flexible, powerful, server-side application for playing music
AddPackage mpv # a free, open source, and cross-platform media player
AddPackage mumble # An Open Source, low-latency, high quality voice chat software (client)
AddPackage recordmydesktop # Produces a OGG encapsulated Theora/Vorbis recording of your desktop
AddPackage syncthing # Open Source Continuous Replication / Cluster Synchronization Thing
AddPackage teeworlds # Fast-paced multiplayer 2D shooter game
AddPackage telegram-desktop # Official Telegram Desktop client
AddPackage vlc # Multi-platform MPEG, VCD/DVD, and DivX player
AddPackage --foreign google-chrome # The popular and trusted web browser by Google (Stable Channel)
AddPackage --foreign rambox-bin # Free and Open Source messaging and emailing app that combines common web applications into one.
AddPackage --foreign spotify # A proprietary music streaming service
AddPackage --foreign teamviewer # All-In-One Software for Remote Support and Online Meetings
AddPackage --foreign zoom # Video Conferencing and Web Conferencing Service
AddPackage qutebrowser # A keyboard-driven, vim-like browser based on PyQt5
AddPackage --foreign brew-git # The missing package manager for macOS (or Linux)
AddPackage lib32-gst-plugins-base-libs # GStreamer Multimedia Framework Base Plugin libraries (32-bit)
AddPackage lib32-gtk3 # GObject-based multi-platform GUI toolkit
AddPackage lib32-libxslt # XML stylesheet transformation library (32-bit)
AddPackage lib32-ocl-icd # OpenCL ICD Bindings (32-bit)
AddPackage lib32-v4l-utils # Userspace tools and conversion library for Video 4 Linux (32-bit)
AddPackage steam # Valve's digital software delivery system
AddPackage steam-native-runtime # Native replacement for the Steam runtime using system libraries
AddPackage wine-staging # A compatibility layer for running Windows programs - Staging branch
