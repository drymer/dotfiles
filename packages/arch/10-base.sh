AddPackage base # Minimal package set to define a basic Arch Linux installation
AddPackage acpi # Client for battery, power, and thermal readings
AddPackage alacritty # A cross-platform, GPU-accelerated terminal emulator
AddPackage alsa-utils # Advanced Linux Sound Architecture - Utilities
IgnorePackage amd-ucode # Microcode update image for AMD CPUs
AddPackage arandr # Provide a simple visual front end for XRandR 1.2.
AddPackage archlinux-themes-slim # Arch Linux branded themes for the SLiM login manager
AddPackage aspell-en # English dictionary for aspell
AddPackage aspell-es # Spanish dictionary for aspell
AddPackage at # AT and batch delayed command scheduling utility and daemon
AddPackage bash # The GNU Bourne Again shell
AddPackage bc # An arbitrary precision calculator language
AddPackage bind # A complete, highly portable implementation of the DNS protocol
AddPackage binutils # A set of programs to assemble and manipulate binary and object files
AddPackage bison # The GNU general-purpose parser generator
AddPackage blueman # GTK+ Bluetooth Manager
AddPackage bluez-hid2hci # Put HID proxying bluetooth HCI's into HCI mode
AddPackage bluez-utils # Development and debugging utilities for the bluetooth protocol stack
AddPackage bzip2 # A high-quality data compression program
AddPackage ccache # Compiler cache that speeds up recompilation by caching previous compilations
AddPackage conntrack-tools # Userspace tools to interact with the Netfilter connection tracking system
AddPackage cryptsetup # Userspace setup tool for transparent encryption of block devices using dm-crypt
AddPackage coreutils # The basic file, shell and text manipulation utilities of the GNU operating system
AddPackage cpio # A tool to copy files into or out of a cpio or tar archive
AddPackage cronie # Daemon that runs specified programs at scheduled times and related tools
AddPackage dash # POSIX compliant shell that aims to be as small as possible
AddPackage dbus # Freedesktop.org message bus system
AddPackage delve # A debugger for the Go programming language.
AddPackage device-mapper # Device mapper userspace library and tools
AddPackage dfc # Display file system space usage using graphs and colors
AddPackage dhcp # A DHCP server, client, and relay agent
AddPackage dhcpcd # RFC2131 compliant DHCP client daemon
AddPackage dmenu # Generic menu for X
AddPackage dos2unix # Text file format converter
AddPackage dunst # Customizable and lightweight notification-daemon
AddPackage e2fsprogs # Ext2/3/4 filesystem utilities
AddPackage efibootmgr # Linux user-space application to modify the EFI Boot Manager
AddPackage encfs # Encrypted filesystem in user-space
AddPackage espeak # Text to Speech engine for English, with support for other languages
AddPackage fakeroot # Tool for simulating superuser privileges
AddPackage file # File type identification utility
AddPackage filesystem # Base Arch Linux files
AddPackage findutils # GNU utilities to locate files
AddPackage gettext # GNU internationalization library
AddPackage glibc # GNU C Library
AddPackage grep # A string search utility
AddPackage grub # GNU GRand Unified Bootloader (2)
AddPackage grub-customizer # A graphical grub2 settings manager
AddPackage gst-libav # Multimedia graph framework - libav plugin
AddPackage gst-plugins-bad # Multimedia graph framework - bad plugins
AddPackage gst-plugins-ugly # Multimedia graph framework - ugly plugins
AddPackage gvfs # Virtual filesystem implementation for GIO
AddPackage gzip # GNU compression utility
AddPackage haveged # Entropy harvesting daemon using CPU timings
AddPackage htop # Interactive process viewer
AddPackage hwinfo # Hardware detection tool from openSUSE
AddPackage i3blocks # Define blocks for your i3bar status line
AddPackage i3-gaps # A fork of i3wm tiling window manager with more features, including gaps
AddPackage i3lock # Improved screenlocker based upon XCB and PAM
AddPackage i3status # Generates status bar to use with i3bar, dzen2 or xmobar
AddPackage inetutils # A collection of common network programs
AddPackage ipcalc # Calculates IP broadcast, network, Cisco wildcard mask, and host ranges
AddPackage iproute2 # IP Routing Utilities
AddPackage iputils # Network monitoring tools, including ping
AddPackage jfsutils # JFS filesystem utilities
AddPackage keychain # A front-end to ssh-agent, allowing one long-running ssh-agent process per system, rather than per login
AddPackage languagetool # An open source language checker
AddPackage less # A terminal based program for viewing text files
AddPackage lib32-gst-plugins-base-libs # GStreamer Multimedia Framework Base Plugin libraries (32-bit)
AddPackage lib32-gtk3 # GObject-based multi-platform GUI toolkit
AddPackage lib32-libxslt # XML stylesheet transformation library (32-bit)
AddPackage lib32-ocl-icd # OpenCL ICD Bindings (32-bit)
AddPackage lib32-v4l-utils # Userspace tools and conversion library for Video 4 Linux (32-bit)
AddPackage libvterm # Abstract library implementation of a VT220/xterm/ECMA-48 terminal emulator
AddPackage licenses # Standard licenses distribution package
AddPackage linux # The Linux kernel and modules
AddPackage linux-firmware # Firmware files for Linux
AddPackage linux-headers # Headers and scripts for building modules for the Linux kernel
AddPackage linux-lts # The LTS Linux kernel and modules
AddPackage logrotate # Rotates system logs automatically
AddPackage lsof # Lists open files for running Unix processes
AddPackage luit # Filter that can be run between an arbitrary application and a UTF-8 terminal emulator
AddPackage lvm2 # Logical Volume Manager 2 utilities
AddPackage lxappearance # Feature-rich GTK+ theme switcher of the LXDE Desktop
AddPackage lxrandr # Monitor configuration tool (part of LXDE)
AddPackage lynx # A text browser for the World Wide Web
AddPackage macchanger # A small utility to change your NIC's MAC address
AddPackage man-db # A utility for reading man pages
AddPackage man-pages # Linux man pages
AddPackage mariadb-clients # MariaDB client tools
AddPackage mdadm # A tool for managing/monitoring Linux md device arrays, also known as Software RAID
AddPackage mediainfo # Supplies technical and tag information about a video or audio file (CLI interface)
AddPackage mediainfo-gui # Supplies technical and tag information about a video or audio file (GUI interface)
AddPackage mlocate # Merging locate/updatedb implementation
AddPackage mousepad # Simple text editor for Xfce
AddPackage netctl # Profile based systemd network management
AddPackage net-tools # Configuration tools for Linux networking
AddPackage networkmanager # Network connection manager and user applications
AddPackage network-manager-applet # Applet for managing network connections
AddPackage nftables # Netfilter tables userspace tools
AddPackage noto-fonts-emoji # Google Noto emoji fonts
AddPackage ntp # Network Time Protocol reference implementation
AddPackage ocl-icd # OpenCL ICD Bindings
AddPackage openssh # Premier connectivity tool for remote login with the SSH protocol
AddPackage openvpn # An easy-to-use, robust and highly configurable VPN (Virtual Private Network)
AddPackage pacman # A library-based package manager with dependency support
AddPackage pacman-contrib # Contributed scripts and tools for pacman systems
AddPackage pandoc # Conversion between markup formats
AddPackage parallel # A shell tool for executing jobs in parallel
AddPackage parcellite # Lightweight GTK+ clipboard manager
AddPackage powerline # Statusline plugin for vim, and provides statuslines and prompts for several other applications, including zsh, bash, tmux, IPython, Awesome, i3 and Qtile
AddPackage nano # Pico editor clone with enhancements
AddPackage ncdu # Disk usage analyzer with an ncurses interface
AddPackage patch # A utility to apply patch files to original sources
AddPackage pavucontrol # PulseAudio Volume Control
AddPackage pciutils # PCI bus configuration space access library and tools
AddPackage perl # A highly capable, feature-rich programming language
AddPackage perlbrew # Manage perl installations in your $HOME
AddPackage picom # X compositor that may fix tearing issues
AddPackage pkgconf # Package compiler and linker metadata toolkit
AddPackage powertop # A tool to diagnose issues with power consumption and power management
AddPackage procps-ng # Utilities for monitoring your system and its processes
AddPackage psmisc # Miscellaneous procfs tools
AddPackage pulseaudio # A featureful, general-purpose sound server
AddPackage pulseaudio-alsa # ALSA Configuration for PulseAudio
AddPackage pv # A terminal-based tool for monitoring the progress of data through a pipeline.
AddPackage read-edid # Program that can get information from a PNP monitor
AddPackage redshift # Adjusts the color temperature of your screen according to your surroundings.
AddPackage reiserfsprogs # Reiserfs utilities
AddPackage reptyr # Utility for taking an existing running program and attaching it to a new terminal
AddPackage rsync # A fast and versatile file copying tool for remote and local files
AddPackage screen # Full-screen window manager that multiplexes a physical terminal
AddPackage scrot # Simple command-line screenshot utility for X
AddPackage sed # GNU stream editor
AddPackage shadow # Password and account management tool suite with support for shadow files and PAM
AddPackage sl # Steam Locomotive runs across your terminal when you type "sl" as you meant to type "ls".
AddPackage slim # Desktop-independent graphical login manager for X11
AddPackage slim-themes # Themes Pack for Simple Login Manager
AddPackage smartmontools # Control and monitor S.M.A.R.T. enabled ATA and SCSI Hard Drives
AddPackage sshfs # FUSE client based on the SSH File Transfer Protocol
AddPackage strace # A diagnostic, debugging and instructional userspace tracer
AddPackage sudo # Give certain users the ability to run some commands as root
AddPackage sxiv # Simple X Image Viewer
AddPackage sysbench # Scriptable multi-threaded benchmark tool for databases and systems
AddPackage sysfsutils # System Utilities Based on Sysfs
AddPackage sysstat # a collection of performance monitoring tools (iostat,isag,mpstat,pidstat,sadf,sar)
AddPackage systemd-sysvcompat # sysvinit compat for systemd
AddPackage tar # Utility used to store, backup, and transport files
AddPackage testdisk # Checks and undeletes partitions + PhotoRec, signature based recovery tool
AddPackage texi2html # Converts texinfo documents to HTML
AddPackage texinfo # GNU documentation system for on-line information and printed output
AddPackage texlive-bibtexextra # TeX Live - Additional BibTeX styles and bibliography databases
AddPackage texlive-core # TeX Live core distribution
AddPackage texlive-fontsextra # TeX Live - all sorts of extra fonts
AddPackage texlive-formatsextra # TeX Live - collection of extra TeX 'formats'
AddPackage texlive-games # TeX Live - Setups for typesetting various board games, including chess
AddPackage texlive-humanities # TeX Live - LaTeX packages for law, linguistics, social sciences, and humanities
AddPackage texlive-latexextra # TeX Live - Large collection of add-on packages for LaTeX
AddPackage texlive-music # TeX Live - Music typesetting packages
AddPackage texlive-pictures # TeX Live - Packages for drawings graphics
AddPackage texlive-pstricks # TeX Live - Additional PSTricks packages
AddPackage texlive-publishers # TeX Live - LaTeX classes and packages for specific publishers
AddPackage texlive-science # TeX Live - Typesetting for mathematics, natural and computer sciences
AddPackage thunar # Modern file manager for Xfce
AddPackage thunar-archive-plugin # Create and extract archives in Thunar
AddPackage thunar-media-tags-plugin # Adds special features for media files to the Thunar File Manager
AddPackage thunar-volman # Automatic management of removeable devices in Thunar
AddPackage thunderbird # Standalone mail and news reader from mozilla.org
AddPackage tk # A windowing toolkit for use with tcl
AddPackage tlp # Linux Advanced Power Management
AddPackage traceroute # Tracks the route taken by packets over an IP network
AddPackage ttf-dejavu # Font family based on the Bitstream Vera Fonts with a wider range of characters
AddPackage ttf-font-awesome # Iconic font designed for Bootstrap
AddPackage tumbler # D-Bus service for applications to request thumbnails
AddPackage unoconv # Libreoffice-based document converter
AddPackage unrar # The RAR uncompression program
AddPackage unzip # For extracting and viewing files in .zip archives
AddPackage usbutils # A collection of USB tools to query connected USB devices
AddPackage util-linux # Miscellaneous system utilities for Linux
AddPackage wget # Network utility to retrieve files from the Web
AddPackage which # A utility to show the full path of commands
AddPackage wireless_tools # Tools allowing to manipulate the Wireless Extensions
AddPackage xautolock # An automatic X screen-locker/screen-saver
AddPackage xcape # Configure modifier keys to act as other keys when pressed and released on their own
AddPackage xf86-input-evdev # X.org evdev input driver
AddPackage xf86-input-synaptics # Synaptics driver for notebook touchpads
AddPackage xf86-video-amdgpu # X.org amdgpu video driver
AddPackage xfburn # A simple CD/DVD burning tool based on libburnia libraries
AddPackage xfconf # Flexible, easy-to-use configuration management system
AddPackage xfdesktop # A desktop manager for Xfce
AddPackage xfsprogs # XFS filesystem utilities
AddPackage xorg-bdftopcf # Convert X font from Bitmap Distribution Format to Portable Compiled Format
AddPackage xorg-iceauth # ICE authority file utility
AddPackage xorg-server # Xorg X server
AddPackage xorg-sessreg # Register X sessions in system utmp/utmpx databases
AddPackage xorg-smproxy # Allows X applications that do not support X11R6 session management to participate in an X11R6 session
AddPackage xorg-x11perf # Simple X server performance benchmarker
AddPackage xorg-xbacklight # RandR-based backlight control application
AddPackage xorg-xcmsdb # Device Color Characterization utility for X Color Management System
AddPackage xorg-xcursorgen # Create an X cursor file from PNG images
AddPackage xorg-xdpyinfo # Display information utility for X
AddPackage xorg-xdriinfo # Query configuration information of DRI drivers
AddPackage xorg-xev # Print contents of X events
AddPackage xorg-xgamma # Alter a monitor's gamma correction
AddPackage xorg-xhost # Server access control program for X
AddPackage xorg-xinput # Small commandline tool to configure devices
AddPackage xorg-xkbevd # XKB event daemon
AddPackage xorg-xkbutils # XKB utility demos
AddPackage xorg-xkill # Kill a client by its X resource
AddPackage xorg-xlsatoms # List interned atoms defined on server
AddPackage xorg-xlsclients # List client applications running on a display
AddPackage xorg-xmodmap # Utility for modifying keymaps and button mappings
AddPackage xorg-xpr # Print an X window dump from xwd
AddPackage xorg-xprop # Property displayer for X
AddPackage xorg-xrdb # X server resource database utility
AddPackage xorg-xrefresh # Refresh all or part of an X screen
AddPackage xorg-xsetroot # Classic X utility to set your root window background to a given pattern or color
AddPackage xorg-xvinfo # Prints out the capabilities of any video adaptors associated with the display that are accessible through the X-Video extension
AddPackage xorg-xwd # X Window System image dumping utility
AddPackage xorg-xwininfo # Command-line utility to print information about windows on an X server
AddPackage xorg-xwud # X Window System image undumping utility
AddPackage xterm # X Terminal Emulator
AddPackage zathura # Minimalistic document viewer
AddPackage zathura-pdf-mupdf # PDF support for Zathura (MuPDF backend) (Supports PDF, ePub, and OpenXPS)
AddPackage zip # Compressor/archiver for creating and modifying zipfiles
AddPackage --foreign backupninja-git # A centralized way to configure and schedule many different backup utilities
AddPackage --foreign electricsheep # Screensaver that realize the collective dream of sleeping computers from all over the internet
AddPackage --foreign bcm20702a1-firmware # Broadcom bluetooth firmware for BCM20702A1 based devices.
AddPackage --foreign openvpn-update-systemd-resolved # OpenVPN systemd-resolved Updater
AddPackage --foreign pacman-fix-permissions # small Python script to fix broken Arch Linux filesystem permissions
AddPackage --foreign pulseaudio-modules-bt-git # PulseAudio Bluetooth modules with SBC, AAC, APTX, APTX-HD, Sony LDAC (A2DP codec) support
AddPackage --foreign ttf-dejavu-sans-mono-powerline-git # DejaVu Sans Mono for Powerline
AddPackage --foreign ttf-font-awesome-4 # 100% free version of font awesome.
AddPackage --foreign unrar-free # Free utility to extract files from RAR archives.
AddPackage --foreign wordnet-cli # A CLI fontend for the WordNet Database
AddPackage --foreign yay # Yet another yogurt. Pacman wrapper and AUR helper written in go.
AddPackage stow # Manage installation of multiple softwares in the same directory tree
AddPackage xclip # Command line interface to the X11 clipboard
AddPackage xfce4-appfinder # An application finder for Xfce
AddPackage xfce4-panel # Panel for the Xfce desktop environment
AddPackage xfce4-power-manager # Power manager for the Xfce desktop
AddPackage xfce4-session # Session manager for Xfce
AddPackage xfce4-settings # Settings manager of the Xfce desktop
AddPackage xfce4-terminal # A modern terminal emulator primarily for the Xfce desktop environment
AddPackage xfwm4 # Xfce's window manager
AddPackage xfwm4-themes # A set of additional themes for the Xfce window manager
AddPackage lib32-gst-plugins-base-libs # GStreamer Multimedia Framework Base Plugin libraries (32-bit)
AddPackage lib32-gtk3 # GObject-based multi-platform GUI toolkit
AddPackage lib32-libxslt # XML stylesheet transformation library (32-bit)
AddPackage lib32-ocl-icd # OpenCL ICD Bindings (32-bit)
AddPackage lib32-v4l-utils # Userspace tools and conversion library for Video 4 Linux (32-bit)
AddPackage rdiff-backup # A utility for local/remote mirroring and incremental backups.
AddPackage --foreign otf-font-awesome-4 # 100% free version of font awesome.
AddPackage --foreign wordnet-common # An Electronic Lexical Database from Princeton University
AddPackage --foreign wordnet-tk # A TK frontend for the WordNet Database
