AddPackage android-tools # Android platform tools
AddPackage arduino # Arduino prototyping platform SDK
AddPackage atom # A hackable text editor for the 21st Century
AddPackage autoconf # A GNU tool for automatically configuring source code
AddPackage automake # A GNU tool for automatically creating Makefiles
AddPackage clang # C language family frontend for LLVM
AddPackage clojure # Lisp dialect for the JVM
AddPackage cmake # A cross-platform open-source make system
AddPackage dia # A GTK+ based diagram creation program
AddPackage diff-so-fancy # Good-looking diffs with diff-highlight and more
AddPackage diffutils # Utility programs used for creating patch files
AddPackage docker # Pack, ship and run any application as a lightweight container
AddPackage docker-compose # Fast, isolated development environments using Docker
AddPackage emacs # The extensible, customizable, self-documenting real-time display editor
AddPackage gcc # The GNU Compiler Collection - C and C++ frontends
AddPackage gcc-libs # Runtime libraries shipped by GCC
AddPackage git # the fast distributed version control system
AddPackage gawk # GNU version of awk
AddPackage go # Core compiler tools for the Go programming language
AddPackage graphviz # Graph visualization software
AddPackage jdk11-openjdk # OpenJDK Java 11 development kit
AddPackage jdk8-openjdk # OpenJDK Java 8 development kit
AddPackage jre11-openjdk-headless # OpenJDK Java 11 headless runtime environment
AddPackage make # GNU make utility to maintain groups of programs
AddPackage minikube # A tool that makes it easy to run Kubernetes locally
AddPackage nodejs # Evented I/O for V8 javascript
AddPackage podman # Tool and library for running OCI-based containers in pods
AddPackage python # Next generation of the python high-level scripting language
AddPackage python2-pip # The PyPA recommended tool for installing Python packages
AddPackage python-pdftotext # Simple PDF text extraction
AddPackage python-pip # The PyPA recommended tool for installing Python packages
AddPackage ruby # An object-oriented language for quick and easy programming
AddPackage rust # Systems programming language focused on safety, speed and concurrency
AddPackage shellcheck # Shell script analysis tool
AddPackage shfmt # Format shell programs
AddPackage stylelint # Mighty, modern CSS linter
AddPackage vagrant # Build and distribute virtualized development environments
AddPackage vi # The original ex/vi text editor
AddPackage vim # Vi Improved, a highly configurable, improved version of the vi text editor
AddPackage virt-manager # Desktop user interface for managing virtual machines
AddPackage virtualbox # Powerful x86 virtualization for enterprise as well as home use
AddPackage virtualbox-host-modules-arch # Virtualbox host kernel modules for Arch Kernel
AddPackage --foreign arduino-mk # A Makefile for Arduino Sketches
AddPackage --foreign drawio-desktop-bin # Diagram drawing application built on web technology
AddPackage --foreign gvm # Go Version Manager
AddPackage --foreign js-beautify # Beautify JavaScript/JSON (jsbeautifier.org)
AddPackage --foreign lens-bin # The Kubernetes IDE
AddPackage --foreign python36 # Major release 3.6 of the Python high-level programming language
AddPackage --foreign python37 # Major release 3.7 of the Python high-level programming language
AddPackage --foreign redis-desktop-manager # Open source cross-platform Redis Desktop Manager based on Qt 5
AddPackage --foreign postman-bin # Build, test, and document your APIs faster
AddPackage eslint # An AST-based pattern checker for JavaScript
AddPackage gopls # Language server for Go programming language
AddPackage python-black # Uncompromising Python code formatter
AddPackage python-language-server # An implementation of the Language Server Protocol for Python
AddPackage tidy # A tool to tidy down your HTML code to a clean style
AddPackage typescript # TypeScript is a language for application scale JavaScript development
AddPackage zshdb # A debugger for zsh scripts.
AddPackage --foreign goreleaser-bin # Deliver Go binaries as fast and easily as possible
AddPackage --foreign vue-cli # Standard tooling for Vue.js development
AddPackage --foreign yaml-language-server-bin # Language server implementation for YAML files and optional schema support
