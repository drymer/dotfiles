# Dotfiles

## TL;DR

Script automatizado:

``` sh
# Hacer dump de los paquetes actuales
bash manage.sh packages-bundle
# Actualizar el SO
bash manage.sh packages-update
# Sincronizar los dotfiles
bash manage.sh dotfiles
```

## Resumen

Mediante aconfmgr, se hace un dump de los paquetes instalados de arch (tanto pacman como yay) para poder gestionar los paquetes de forma declarativa

Además, se gestionan los dotfiles con stow.
