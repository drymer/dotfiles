#!/usr/bin/env bash
set -eo pipefail

arg=$1

if [[ $arg == "install-packages" ]]
then
    # Instalar yay
    if ! command -v yay &> /dev/null
    then
       sudo pacman -S --needed git base-devel
       dotfiles_path=$(pwd)
       mkdir -p $HOME/Instalados
       cd $HOME/Instalados
       git clone https://aur.archlinux.org/yay.git
       cd yay
       makepkg -si
       cd $dotfiles_path
    fi

    # Instalar aconfmgr
    if ! command -v aconfmgr &> /dev/null
    then
        yay -S aconfmgr-git
    fi

    # Instalar homebrew
    if [[ -f brew ]]
    then
        yay -S brew-git
        sudo chown -R $(whoami).users /opt/brew/
    fi

    # Instalar oh-my-zsh
    if [[ ! -d $HOME/.oh-my-zsh ]]; then
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    fi

    echo Todo está instalado.

elif [[ $arg == "diff-bundle" ]]
then
    # Guarda los paquetes actuales, si hay diferencias creará un 99-unsorted.sh
    echo Comprobando los paquetes del sistema...
    aconfmgr -c packages/arch save 2> /dev/null
    if [[ -e packages/arch/99-unsorted.sh ]]
    then
        echo Hay diferencias en los paquetes instalados, deberias quitarlos o añadirlos a los demás ficheros. Cuando estés, borra el fichero de unsorted.
        cat packages/arch/99-unsorted.sh
    fi

    # Guarda los paquetes de brew actuales
    brew bundle dump --force --file /tmp/Brewfile
    # No se puede controlar que diff "falle" cuando hay diferencias, así que desactivamos ese control temporalmente
    set +e
    brew_diffs=$(diff -q /tmp/Brewfile packages/brew/Brewfile)
    set -e
    if [[ -n $brew_diffs  ]]
    then
        echo $brew_diffs
        echo Si estás de acuerdo con los cambios, puedes guardarlos ejecutando "brew bundle dump --force --file packages/brew/Brewfile"
        echo Si no lo estás, ejecuta "brew bundle --file packages/brew/Brewfile --force cleanup"
    fi

elif [[ $arg == "apply-bundle" ]]
then
    # Actualizar repositorios
    sudo pacman -Sy

    # Instalar paquetes del sistema
    aconfmgr -c packages/arch apply

    # Instalar paquetes de brew
    brew bundle install --file packages/brew/Brewfile

elif [[ $arg == "update-bundle" ]]
then
    # Actualiza oh my zsh
    sh "$HOME/.oh-my-zsh/tools/upgrade.sh"
    rm -rf "$HOME/.oh-my-zsh/log/update.lock"

    # Actualiza arch
    sudo pacman -Syu

    # Sincroniza los repositorios de brew
    brew update

    # Actualiza los paquetes instalados
    brew upgrade

elif [[ $arg == "dotfiles" ]]
then
    cd dotfiles
    stow -t ~ git
    stow -t ~ i3
    stow -t ~ shells
    cd ..
else
    echo "Usa 'install-packages', 'diff-bundle', 'apply-bundle', 'update-bundle' o 'dotfiles'"
fi
